
const { transaction, transtype, Account,Op } = require('./../config/sequelieze.conf')
async function getTransactions(filter, data) {
    let datos = await transaction.findAll({
        include: {
            model: transtype,
            where: filter
        }
    })
    data(datos)
}

function gettransaction(cif, data) {
    transaction.findAll({
        where: {
            [Op.or]:[{accountNumberCredit: cif},{accountNumberDebit: cif}]            
        }
    }).then((datos) => {
        data(datos)
    })
}

async function inputtransaction(data, callback) {

    if (data.transactionType == '002') {

        let accdebit = await Account.findOne({
            where: {
                accountNumber: data.accountNumberDebit
            }
        })

        if (accdebit) {
            let newbalance = accdebit.balance -= data.amount
            Account.update(
                {balance: newbalance},
                {where:
                    {accountNumber:data.accountNumberDebit}
            }).then((data) => {
                if (data) {
                    transaction.create({
                        transaction_id: data.transaction_id,
                        accountNumberCredit: data.accountNumberCredit,
                        accountNumberDebit: data.accountNumberDebit,
                        amount: data.amount,
                        transactionType: data.transactionType,
                        date: new Date(),
                    }).then((datos) => {
                        console.log(datos)
                        callback(datos.get())
                    })
                }
            })
        }
    } else if (data.transactionType == '003') {

        let acccredit = await Account.findOne({
            where: {
                accountNumber: data.accountNumberCredit
            }
        })

        if (acccredit){
            let newcredit=acccredit+=data.amount
            Account.update(
                {balance:newcredit},
                {where:{
                    accountNumberCredit: data.accountNumberCredit
                }

            })
        }

    } else {
        let accdebit = await Account.findOne({
            where: {
                accountNumber: data.accountNumberDebit
            }
        })

        let acccredit = await Account.findOne({
            where: {
                accountNumber: data.accountNumberCredit
            }
        })

        if (accdebit) {
            if (acccredit) {
                let debitbalance = accdebit.balance -= data.amount
                let creditbalance = acccredit.balance += data.amount
                Account.update(
                    {
                        balance: debitbalance
                    },
                    {
                        where: {
                            accountNumber: accdebit.accountNumber
                        }
                    }
                ).then((datos) => {
                    console.log(datos)
                    Account.update(
                        { balance: creditbalance },
                        {
                            where: {
                                accountNumber: acccredit.accountNumber
                            }
                        }).then((datos) => {
                            console.log(datos)
                            transstatus = true
                            if (transstatus) {
                                transaction.create({
                                    transaction_id: data.transaction_id,
                                    accountNumberCredit: data.accountNumberCredit,
                                    accountNumberDebit: data.accountNumberDebit,
                                    amount: data.amount,
                                    date: new Date(),
                                    transactionType: data.transactionType
                                }).then((datos) => {
                                    console.log(datos)
                                    callback(datos.get())
                                })
                            }
                        })
                })
            }
        }
    }
}


module.exports = { getTransactions, gettransaction, inputtransaction };