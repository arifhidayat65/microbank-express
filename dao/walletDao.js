
const { wallet, walletaccount } = require('./../config/sequelieze.conf')
function getwallets(data) {
    wallet.findAll({
        include: {
            model: walletaccount
        }
    }).then((datos) => {
        data(datos)
    })
}

function getwallet(cif, data) {
    wallet.findOne({
        where: {
            walletid: cif
        }
    }).then((datos) => {
        data(datos)
    })
}

function getwalletaccount(cif, data) {
    wallet.findAll({
        include: [{
            model: walletaccount,
            where: {
                accountNumber:cif
            }
        }]
    }).then((datos) => {
        data(datos)
    })
}

function inputwallet(data,callback) {
    wallet.create({
        walletid:data.walletid,
        description:data.description,
        createdDate:new Date(),
    }).then((datos)=>{
        callback(datos)
        callback(datos.get())
        walletaccount.create({
            walletid:datos.walletid,
            accountNumber:data.accountNumber
        }).then((data)=>{
            console.log(data.get())
        })
    })
    
}

function editwallet(data,callback) {
//     wallet.update(
//         {
//         firstName:data.firstName,
//         lastName:data.lastName,
//         birthDate:data.birthDate,
//         email:data.email,
//         password:data.password,
//     },{where:{walletNumber:data.walletNumber}}
// ).then((datos)=>{
//         callback(datos)
//     })
}

function deletewallet(cif,data) {
    // wallet.destroy({
    //     where:{walletNumber:cif}
    // }).then(()=>{
    //     data(null)
    // })
}

module.exports = { getwallets, getwalletaccount, getwallet, inputwallet, deletewallet, editwallet };