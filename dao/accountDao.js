
const { Customer, Account } = require('./../config/sequelieze.conf')
function getaccounts(filter, data) {
    Account.findAll({
        include: [{
            model: Customer,
            as: 'customer'
        }],
        where:filter
    }).then((datos) => {
        data(datos)
    })
}

function getaccount(accnum, data) {
    // Account.findOne(
    //     {
    //         include: [{
    //             model: Customer,
    //             as: 'customer'
    //         }],
    //     where: {
    //         accountNumber: accnum
    //     }
    // }).then((datos) => {
    //     data(null, datos)
    // })

    Account.findAll(
        {
            // include: [{
            //     model: Customer,
            //     as: 'customer'
            // }],
        where: {
            customerNumber: accnum
        }
    }).then((datos) => {
        datos.forEach(element => {
            if(element.accountType=='001'){
                element.accountType='main'
            }else if (element.accountType=='002'){
                element.accountType='virtual'
            }else{
                element.accountType='mini'
            }
        });
        data(null, datos)
    })
}

function inputaccount(data,callback) {
    // console.log(data)
    Account.create({
        accountNumber:data.accountNumber,
        accountName:data.accountName,
        accountType:data.accountType,
        balance:data.balance,
        openDate:new Date(),
        customerNumber:data.customerNumber
    }).then((datos)=>{
        console.log(datos)
        callback(null,datos.get())
    })
}

function editaccount(accnum,data,callback) {
    Account.update(
        {
            accountName:data.accountName,
            accountType:data.accountType,
            balance:data.balance,
            openDate:new Date(),
    },{where:{accountNumber:accnum}}
).then((datos)=>{
        callback(null,datos)
    })
}

function deleteaccount(cif,data) {
    Account.destroy({
        where:{accountNumber:cif}
    }).then(()=>{
        data(null,{"message":"data deleted"})
    })
}

module.exports = { getaccounts, getaccount, inputaccount, deleteaccount, editaccount };