
const { Customer, Account } = require('./../config/sequelieze.conf')
function getcustomers(data) {
    Customer.findAll({
        include: [{
            model: Account,
            as: 'accounts'
        }]
    }).then((datos) => {
        data(datos)
    }).catch((err)=>{
        data(err)
    })
}

function getcustomer(cif, data) {
    Customer.findOne({
        where: {
            customerNumber: cif
        }
    }).then((datos) => {
        data(datos)
    })
}

function getcustomeraccount(cif, data) {
    Customer.findAll({
        include: [{
            model: Account,
            as: 'accounts'
        }],
        where: {
            customerNumber: cif
        }
    }).then((datos) => {
        data(datos)
    })
}

function inputcustomer(data,callback) {
    Customer.create({
        customerNumber:data.customerNumber,
        firstName:data.firstName,
        lastName:data.lastName,
        birthDate:data.birthDate,
        email:data.email,
        password:data.password
    }).then((datos)=>{
        console.log(datos)
        callback(datos.get())
    })
}

function editcustomer(data,callback) {
    Customer.update(
        {
        firstName:data.firstName,
        lastName:data.lastName,
        birthDate:data.birthDate,
        email:data.email,
        password:data.password,
    },{where:{customerNumber:data.customerNumber}}
).then((datos)=>{
        callback(datos)
    })
}

function deletecustomer(cif,data) {
    Customer.destroy({
        where:{customerNumber:cif}
    }).then(()=>{
        data(null)
    })
}

module.exports = { getcustomers, getcustomeraccount, getcustomer, inputcustomer, deletecustomer, editcustomer };