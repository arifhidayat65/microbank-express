
const { Customer, Account } = require('./../config/sequelieze.conf')
function getcustomers(data) {
    Customer.findAll({
        include: [{
            model: Account,
            as: 'accounts'
        }]
    }).then((datos) => {
        data(datos)
    })
}

function getcustomer(cif, data) {
    Customer.findOne({
        where: {
            customerNumber: cif
        }
    }).then((datos) => {
        data(datos)
    })
}

function getcustomeraccount(cif, data) {
    Customer.findOne({
        include: [{
            model: Account,
            as: 'accounts'
        }],
        where: {
            customerNumber: cif
        }
    }).then((datos) => {
        data(datos)
    })
}

async function inputcustomer(data,callback) {

    let oldcif=await Customer.findOne({
        order:[
            ['customer_number','DESC']
        ]
    })
    let ncif=oldcif.customerNumber.substring(5,oldcif.customerNumber.length)
    callback(Number(ncif)+1)
    // Customer.create({
    //     customerNumber:data.customerNumber,
    //     firstName:data.firstName,
    //     lastName:data.lastName,
    //     birthDate:data.birthDate,
    //     email:data.email,
    //     password:data.password
    // }).then((datos)=>{
    //     console.log(datos)
    //     callback(datos.get())
    // })
}

function editcustomer(data,callback) {
    Customer.update(
        {
        firstName:data.firstName,
        lastName:data.lastName,
        birthDate:data.birthDate,
        email:data.email,
        password:data.password,
    },{where:{customerNumber:data.customerNumber}}
).then((datos)=>{
        callback(datos)
    })
}

function deletecustomer(cif,data) {
    Customer.destroy({
        where:{customerNumber:cif}
    }).then(()=>{
        data(null)
    })
}

function login(email,upass,callback){
    Customer.findOne({
        where:{
            email:email
        }
    }).then((data)=>{
        if (data.password==upass){
            callback(data)
        }else{
            callback(null)
        }
    })
}
module.exports = { getcustomers, getcustomeraccount, getcustomer, inputcustomer, deletecustomer, editcustomer,login };