module.exports = (sequelize, DataTypes) => {
    const Transaction = sequelize.define('transaction', {
        transactionId:{
            field:'transaction_id',
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        accountNumberCredit: {
            field: 'account_number_credit',
            type: DataTypes.STRING
        },
        accountNumberDebit: {
            field: 'account_number_debit',
            type: DataTypes.STRING
        },
        amount:{
            field:'amount',
            type:DataTypes.INTEGER
        },
        date:{
            field:'date',
            type:DataTypes.DATE
        },
        transactionType:{
            field:'transactionType',
            type:DataTypes.STRING
        }
    },{
            tableName: 'transaction',
            timestamps: false
        });

    return Transaction;
};
