module.exports = (sequelize, DataTypes) => {
    const TransactionType = sequelize.define('TransactionType', {
        id:{
            field:'id',
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            field: 'code',
            type: DataTypes.STRING
        },
        description: {
            field: 'description',
            type: DataTypes.STRING
        },
    }, {
            tableName: 'transaction_type',
            timestamps: false
        });

    return TransactionType;
};
