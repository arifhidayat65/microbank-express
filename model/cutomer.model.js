module.exports = (sequelize, DataTypes) => {
    const Customer = sequelize.define('customer', {
        customerNumber: {
            field: 'customer_number',
            type: DataTypes.STRING,
            primaryKey: true
        },
        firstName: {
            field: 'first_name',
            type: DataTypes.STRING
        },
        lastName: {
            field: 'last_name',
            type: DataTypes.STRING
        },
        birthDate: {
            field: 'birth_date',
            type: DataTypes.DATE
        },
        email: {
            field: 'email',
            type: DataTypes.STRING
        },
        password: {
            field: 'password',
            type: DataTypes.STRING
        }
    }, {
            tableName: 'customer',
            timestamps: false
        });

    return Customer;
};
