module.exports = (sequelize, DataTypes) => {
    const Account = sequelize.define('account', {
        accountNumber: {
            field: 'account_number',
            type: DataTypes.STRING,
            primaryKey: true,
            autoIncrement: true
        },
        accountName: {
            field: 'account_name',
            type: DataTypes.STRING
        },
        accountType: {
            field: 'account_type',
            type: DataTypes.STRING
        },
        balance: {
            field: 'balance',
            type: DataTypes.INTEGER
        },
        openDate: {
            field: 'open_date',
            type: DataTypes.DATE
        },
        customerNumber: {
            field: 'customer_number',
            type: DataTypes.STRING
        }
    }, {
            tableName: 'account',
            timestamps: false
        });

    return Account;
};
