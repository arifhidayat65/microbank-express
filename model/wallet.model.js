module.exports = (sequelize, DataTypes) => {
    const Wallet = sequelize.define('wallet', {
        walletid: {
            field: 'wallet_id',
            type: DataTypes.STRING,
            primaryKey: true
        },
        description: {
            field: 'description',
            type: DataTypes.STRING
        },
        createdDate: {
            field: 'created_date',
            type: DataTypes.DATE
        },
        // customerNumber: {
        //     field: 'customer_number',
        //     type: DataTypes.STRING
        // },
    }, {
            tableName: 'wallet',
            timestamps: false
        });

    return Wallet;
};
