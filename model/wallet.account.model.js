module.exports = (sequelize, DataTypes) => {
    const WalletAccount = sequelize.define('wallet_account', {
        id:{
            field:'id',
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        walletid: {
            field: 'wallet_id',
            type: DataTypes.STRING
        },
        accountNumber: {
            field: 'account_number',
            type: DataTypes.STRING
        },
    }, {
            tableName: 'wallet_account',
            timestamps: false
        });

    return WalletAccount;
};
