const Sequelize = require('sequelize');
const CustomerModel = require('./../model/cutomer.model');
const AccountModel = require('./../model/account.model')
const TransactionModel = require('../model/transaction.model')
const TranstypeModel = require('./../model/transaction.type.model')
const WalletModel = require('./../model/wallet.model')
const walletAccpuntModel = require('./../model/wallet.account.model')
const Op = Sequelize.Op
const sequelize = new Sequelize('db_microbank', 'root', 'admin123', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});
const Customer = CustomerModel(sequelize, Sequelize);
const Account = AccountModel(sequelize, Sequelize)
const transaction = TransactionModel(sequelize, Sequelize)
const transtype = TranstypeModel(sequelize, Sequelize)
const wallet = WalletModel(sequelize, Sequelize)
const walletaccount = walletAccpuntModel(sequelize, Sequelize)


Account.belongsTo(Customer, {
  foreignKey: 'customer_number'
});
Customer.hasMany(Account, {
  foreignKey: 'customer_number'
});
transaction.hasOne(transtype, {
  foreignKey: 'code',
  sourceKey: 'transactionType'
})
wallet.hasMany(walletaccount, {
  foreignKey: 'wallet_id'
})
walletaccount.belongsTo(wallet, {
  foreignKey: 'wallet_id'
})
module.exports = {
  Customer,
  Account,
  transaction,
  transtype,
  wallet,
  walletaccount,
  Op
};