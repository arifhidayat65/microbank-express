const express=require('express')
const app=express()
const port=1000
const customerRoute=require('./controller/customerroute')
const accountRoute=require('./controller/accountroute')
const transactionroute=require('./controller/transactionroute')
const walletroute=require('./controller/walletroute')
app.use(express.json())
app.use(customerRoute).use(accountRoute).use(transactionroute).use(walletroute)

app.listen(port)