const Router = require('express').Router()
const customerService=require('./../dao/customerDao')
const Resp=require('./../dto/response')
Router.get('/customers',(req,res,next)=>{
    customerService.getcustomers((datos)=>{
        Resp.OK(res,datos)
    })
})

Router.get('/customer/:cif',(req,res,next)=>{
    customerService.getcustomer(req.params.cif,(datos)=>{
       Resp.OK(res,datos)
    //    res.json(datos)
    })
})


Router.post('/customer',(req,res,next)=>{
    customerService.inputcustomer(req.body,(data)=>{
        Resp.OK(res,data)
    })
})

Router.put('/customer/:cif',(req,res,next)=>{
    customerService.editcustomer(req.params.cif,req.body,(data)=>{
        Resp.OK(res,data)
    })
})

Router.delete('/customer/:cif',(req,res,next)=>{
    customerService.deletecustomer(req.params.cif,(data)=>{
        Resp.OK(res,data)
    })
})

Router.get('/customer/:cif/accounts',(req,res,next)=>{
    customerService.getcustomeraccount(req.params.cif,(data)=>{
        Resp.OK(res,data)
    })
})

Router.post('/login',(req,res,next)=>{
    customerService.login(req.body.email,req.body.password,(data)=>{
        if (data){
            Resp.OK(res,data)
        }else{
            Resp.NotFound(res,data)
        }
    })
})
module.exports=Router