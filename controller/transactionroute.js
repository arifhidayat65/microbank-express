const Router = require('express').Router()
const transacstionService=require('../dao/transactionDao')
const Resp=require('../dto/response')
Router.get('/transactions',(req,res,next)=>{
    let filter={};
    if (req.query.transtype){
        filter.description=req.query.transtype
    }
    transacstionService.getTransactions(filter,(data)=>{
        Resp.OK(res,data)
    })
})

Router.get('/transaction/:accnum',(req,res,next)=>{
    // accountService.getaccount(req.params.accnum,(err,data)=>{
    //     Resp.OK(res,data)
    // }) 
    transacstionService.gettransaction(req.params.accnum,(data)=>{
        Resp.OK(res,data)
    })
})


Router.post('/transaction',(req,res,next)=>{
transacstionService.inputtransaction(req.body,(data)=>{
    Resp.OK(res,data)
})    
})

// Router.put('/account',(req,res,next)=>{
    
// })

// Router.delete('/account',(req,res,next)=>{
    
// })

module.exports=Router