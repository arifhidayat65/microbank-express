const Router = require("express").Router();
const accountService = require("./../dao/accountDao");
const Resp = require("./../dto/response");
Router.get("/accounts", (req, res, next) => {
  let filter = {};
  if (req.query.balance) {
    filter.balance = req.query.balance;
  }
  accountService.getaccounts(filter, data => {
    Resp.OK(res, data);
  });
});

Router.get("/account/:accnum", (req, res, next) => {
  accountService.getaccount(req.params.accnum, (err, data) => {
    Resp.OK(res, data);
  });
});

Router.post("/account", (req, res, next) => {
  accountService.inputaccount(req.body, (err, data) => {
    Resp.OK(res, data);
  });
});

Router.put("/account", (req, res, next) => {});

Router.delete("/account", (req, res, next) => {});

module.exports = Router;