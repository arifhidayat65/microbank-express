const Router = require('express').Router()
const walletService=require('./../dao/walletDao')
const Resp=require('./../dto/response')
Router.get('/wallets',(req,res,next)=>{
    walletService.getwallets((data)=>{
        Resp.OK(res,data)
    })
})

Router.get('/wallet/:accnum',(req,res,next)=>{
    walletService.getwalletaccount(req.params.accnum,(data)=>{
        Resp.OK(res,data)
    }) 
})


Router.post('/wallet',(req,res,next)=>{
    walletService.inputwallet(req.body,(datos)=>{
        Resp.OK(res,datos)
    })    
})

// Router.put('/account',(req,res,next)=>{
    
// })

// Router.delete('/account',(req,res,next)=>{
    
// })

module.exports=Router